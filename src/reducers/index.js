import {combineReducers} from "redux";
import {task} from "./task_reducer";

export default combineReducers({
    task
});