import {
    ADD_TASK
} from '../actions/types';

const initialState = {
    tasks: [
        {
            title: "menage",
            isDone: false
        },
        {
            title: "vaisselle",
            isDone: true
        }
    ]
};

export const task = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASK:
            return {
                ...state,
                tasks: action.task || [],
            };
        default:
            return state;
    }
};
