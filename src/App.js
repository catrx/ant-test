import React from 'react';
import MainRouting from "./router/main_routing";
import 'antd/dist/antd.css';
import './assets/styles/styles.css';
import {Provider} from 'react-redux'
import {createStore} from "redux";
import reducers from './reducers';

const store = createStore(
    reducers,
);

function App() {
    return (
        <Provider store={store}>
            <MainRouting/>
        </Provider>
    );
}

export default App;
