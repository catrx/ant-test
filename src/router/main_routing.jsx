import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Home from "../components/home/home";
import MenuComponent from "../components/menu/menu";
import {Col, Row} from "antd";

const MainRouting = () => {
    return (
        <Router>
            <Row>
                <Col span={4}>
                    <MenuComponent/>
                </Col>
                <Col span={18} className="app-content">
                    <Switch>
                        <Route path="/" component={Home}/>
                    </Switch>
                </Col>
            </Row>
        </Router>
    )
}

export default MainRouting