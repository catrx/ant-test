import React from "react";
import {useSelector} from "react-redux";
import { Row, Col } from 'antd';
import Item from "../item/item";

const List = () => {
    const { tasks} = useSelector(state => state.task)

    console.log(tasks)

    if(!tasks) {
        return null;
    }
    return(
        <Row >
            {tasks.map(task => (
                <Col key={`task_${task.title}`} style={{padding: 8}} className="gutter-row" span={6}>
                    <Item {...{ task }} />
                </Col>
            ))}
        </Row>
    )
}

export default List